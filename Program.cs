﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace JusanGarantWin
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            // Чтение файла
            List<Contractor> contractors = readFile();

            // Запись в файл
            writeToFile(contractors);


            List<string> individualsNames = new List<string>();
            List<string> companyNames = new List<string>();
            foreach (Contractor contractor in contractors)
            {
                if (contractor is IndividContractor)
                {
                    IndividContractor temp = (contractor as IndividContractor);
                    individualsNames.Add(temp.Name + " " + temp.Surname + " " + temp.Patronymic);
                }
                else
                {
                    LegalContractor temp = (contractor as LegalContractor);
                    companyNames.Add(temp.CompanyName);
                }
            }

            // вывод имен физлиц отсортированные
            List<string> individualsNamesSorted = individualsNames.OrderBy(q => q).ToList();
            Console.WriteLine("INDIVIDUALS NAMES:");
            foreach (var name in individualsNamesSorted)
            {
                Console.WriteLine(name);
            }


            // вывод названии компании отсортированные
            List<string> companyNamesSorted = companyNames.OrderBy(q => q).ToList();
            Console.WriteLine("COMPANY NAMES:");
            foreach (var name in companyNamesSorted)
            {
                Console.WriteLine(name);
            }
        }

        private static List<Contractor> readFile()
        {
            var result = new List<Contractor>();
            try
            {
                /*
                 *  Формат для ип:
                    ИП "Название ип" биин 
                    Формат для физлиц:
                    Имя Фамилия Отчество иин гендер
                    -------------------------------
                    Отчество может отсутствовать
                    Длина иина должна быть 12 символов
                    Название ип без пробела должна быть
                 */
                StreamReader sr = new StreamReader(@"..\..\input.json");
                string json = sr.ReadToEnd();
                ContractorJson contractorJson = JsonConvert.DeserializeObject<ContractorJson>(json);
                foreach (var individ in contractorJson.individs)
                {
                    if (isValidIin(individ.biin))
                    {
                        Contractor contractor = new IndividContractor(individ.name, individ.surname, individ.patronymic,
                            individ.biin);
                        result.Add(contractor);
                    }
                    else
                    {
                        Console.WriteLine("WRONG IIN " + individ.biin);
                    }
                }

                foreach (var legal in contractorJson.legals)
                {
                    if (isValidIin(legal.biin))
                    {
                        Contractor contractor = new LegalContractor(legal.companyName, legal.biin);
                        result.Add(contractor);
                    }
                    else
                    {
                        Console.WriteLine("WRONG IIN " + legal.biin);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("FILE NOT FOUNT, PLEASE EDIT WORK DIRECTORY OF RIDER");
            }

            return result;
        }

        private static bool isValidIin(string iin)
        {
            return iin.Length == 12;
        }

        private static void writeToFile(List<Contractor> list)
        {
            try
            {
                /*
                 *  Формат для ип:
                    ИП "Название ип" биин 
                    Формат для физлиц:
                    Имя Фамилия Отчество иин гендер
                    -------------------------------
                    Отчество может отсутствовать
                    Длина иина должна быть 12 символов
                    Название ип без пробела должна быть
                 */
                using (StreamWriter sr = new StreamWriter(@"..\..\output.json"))
                {
                    ContractorJson json = new ContractorJson();
                    List<ContractorJsonIndivid> individs = new List<ContractorJsonIndivid>();
                    List<ContractorJsonLegal> legals = new List<ContractorJsonLegal>();


                    foreach (var contractor in list)
                    {
                        if (contractor is IndividContractor)
                        {
                            IndividContractor temp = (contractor as IndividContractor);
                            ContractorJsonIndivid i = new ContractorJsonIndivid();
                            i.name = temp.Name;
                            i.surname = temp.Surname;
                            i.patronymic = temp.Patronymic;
                            i.biin = temp.biin;
                            individs.Add(i);
                        }
                        else
                        {
                            LegalContractor temp = (contractor as LegalContractor);
                            ContractorJsonLegal l = new ContractorJsonLegal();
                            l.companyName = temp.CompanyName;
                            l.biin = temp.biin;
                            legals.Add(l);
                        }

                        json.individs = individs;
                        json.legals = legals;
                    }

                    string jsonData = JsonConvert.SerializeObject(json, Formatting.None);
                    sr.Write(jsonData);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("CAN NOT CREATE FILE");
            }
        }
    }
}