﻿using System.Collections.Generic;

namespace JusanGarantWin
{
    
        public class ContractorJson
        {
            public List<ContractorJsonIndivid> individs;
            public List<ContractorJsonLegal> legals;
        }

        public class ContractorJsonIndivid
        {
            public string name;
            public string surname;
            public string patronymic;
            public string biin;
        }

        public class ContractorJsonLegal
        {
            public string companyName;
            public string biin;
        }
    }
