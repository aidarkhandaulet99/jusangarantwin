﻿using System;
namespace JusanGarantWin
{
    public abstract class Contractor
    {
        public abstract ContractorType contractorType { get; }
        public string biin { get; set; }
        public DateTime createdDate { get; set; }
        public DateTime updatedDate { get; set; }
        public string updatedAuthor { get; set; }

        protected Contractor(string biin)
        {
            this.biin = biin;
            updatedAuthor = "Daulet Aidarkhan";
            createdDate = DateTime.Now;
            updatedDate = DateTime.Now;
        }
        
    }
}
