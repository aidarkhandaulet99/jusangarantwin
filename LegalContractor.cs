﻿using System;
using System.Collections.Generic;

namespace JusanGarantWin
{
    public class LegalContractor : Contractor
    {
        public string CompanyName { get; set; }
        public List<IndividContractor> contacts{ get; set; }

        public LegalContractor(
            string companyName,
            string biin
        ) : base(biin)
        {
            CompanyName = companyName;
        }

        public void addContact(IndividContractor contact)
        {
            contacts.Add(contact);
            updatedDate = DateTime.Now;
        }

        public override ContractorType contractorType => ContractorType.LEGAL;
    }
}